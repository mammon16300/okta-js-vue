FROM node:12-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache bash git openssh

RUN mkdir /app
WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

RUN npm run postinstall
RUN npm run preresource-server

ENV NODE_ENV production

EXPOSE 8080

CMD ["npm", "run", "custom-login-server"]
